# drinkr
A Star-Trek-themed prototype for ordering (and reordering) beverages from various bars.

### Development build
- From the project directory:
	- `npm install`
	- `npm run start`
- The app will open automatically in default browser

### Production build
- From the project directory:
	- `npm run build`
	- `serve -s build` (if needed, install _serve_ with `npm install -g serve`)
- Open a new tab and navigate to the localhost:5000

### Assumptions
- **Scaling:** Folder structure was created with the option for scaling the app.
- **Accessibility:** Accessibility wasn't considered as this app is a prototype.
- **Testing:** Testing wasn't implemented due to a time limitations.
- **Data:** Order history is accessible using persisted state. Ideally, this would be saved/retrieved using the agreed upon API.
- **Styling:** I included both the sass and their compiled css files, in case it is helpful to see my work flow.
