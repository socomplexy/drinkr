import {
	DRINK_ADDED,
	DRINK_CREATED,
	DRINK_REMOVED,
	BAR_SELECTED,
	ORDER_COMPLETED
} from "../utils/constants"

const initialState = {
	pending: false,
	bar: null,
	total: 0,
	count: 0,
	drinks: {},
	previousOrders: [],
	error: null
}

const order = (state = initialState, action) => {
	switch (action.type) {
		case BAR_SELECTED :
			return {
				...state,
				pending: true,
				bar: action.bar,
				total: 0,
				count: 0,
				drinks: {},
			}
		case DRINK_CREATED :
			return {
				...state,
				drinks: {...state.drinks,
					[action.drink.id]: {
					price: action.drink.price,
					quantity: 0
				}}
			}
		case DRINK_ADDED :
			return {
				...state,
				total: state.total + action.drink.price,
				count: state.count+1,
				drinks: {...state.drinks,
					[action.drink.id]: {
					...state.drinks[action.drink.id],
					quantity: state.drinks[action.drink.id].quantity+1
				}}
			}
		case DRINK_REMOVED :
			return {
				...state,
				pending: true,
				total: state.total-action.drink.price,
				count: state.count-1,
				drinks: {...state.drinks,
					[action.drink.id]: {
					...state.drinks[action.drink.id],
					quantity: state.drinks[action.drink.id].quantity-1
				}}
			}
		case ORDER_COMPLETED :
			return {
				...state,
				pending: false,
				bar: null,
				total: 0,
				count: 0,
				drinks: {},
				previousOrders: [...state.previousOrders, action.order],
				error: null
			}
		default :
			return state
	}
}

export default order
