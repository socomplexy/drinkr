import React from 'react'
import { render } from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import { createLogger } from 'redux-logger'
import thunk from 'redux-thunk'

import reducer from './reducers'
import App from './App'
import './styles.css'

const middleWare = [ thunk ]
const persistedState = localStorage.getItem('reduxState') ? JSON.parse(localStorage.getItem('reduxState')) : {}

if (process.env.NODE_ENV !== 'production') {
	middleWare.push(createLogger())
}

const store = createStore(
	reducer,
	persistedState,
	applyMiddleware(...middleWare)
)

store.subscribe(() => {
	localStorage.setItem('reduxState', JSON.stringify(store.getState()))
})

render(
	<Provider store={store}>
		<BrowserRouter>
			<App />
		</BrowserRouter>
	</Provider>
	, document.getElementById('root')
)
