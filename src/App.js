import React from 'react'
import { Link, Route, Switch, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'

import { completeOrder } from './actions'
import { copy } from './utils/copy'
import Bar from './containers/bar'
import Browse from './components/browse/Browse'
import Dashboard from './components/dashboard/Dashboard'
import Previous from './containers/previous'
import Search from './components/search/Search'


const App = ({ completeOrder }) => (
	<div className="App">
		<header className="App-header">
			<Link to='/'>
				<img src='/images/logo.png' alt='' />
				<h1 className="title">{copy.appName}</h1>
			</Link>
		</header>

		<Switch>
			<Route path='/' exact component={Dashboard} />
			<Route path='/search' component={Search} />
			<Route path='/browse' component={Browse} />
			<Route path='/previous' component={Previous} />
			<Route path='/:bar'
				component={Bar}
				completeOrder={completeOrder} />
		</Switch>

	</div>
)

const mapStateToProps = state => ({ order: state.order })
const mapDispatchToProps = dispatch => ({
	completeOrder: order => dispatch(completeOrder(order)),
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App))
