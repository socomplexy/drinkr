export const copy = {
	appName: "drinkr",
	barLabel: "Bar:",
	browse: "Browse",
	browseBars: "Browse Bars",
	completeOrder: "Complete Order",
	dashboardTitle: "What would you like to do?",
	dateLabel: "Date:",
	findEstablishment: "Find an establishment",
	itemsOrderedLabel: "Items Ordered",
	menu: "Menu",
	orderAgain: "Order again",
	previousOrders: "Previous Orders",
	search: "Search",
	totalLabel: "Total:",
	viewMenu: "View menu",
}