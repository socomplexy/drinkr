import React, { Component } from 'react'

import bars from '../../data/bars.json'
import { copy } from '../../utils/copy'

import BrowseListItem from '../../components/browseListItem/BrowseListItem'
import SearchIcon from 'material-ui-icons/Search'
import TextField from 'material-ui/TextField'
import './styles.css'

class Search extends Component {
	state = {
		searchInput: '',
		searchResults: {},
	}

	updateResults = () => {
		let results = {}

		Object.keys(bars).forEach(bar => {
			if(bars[bar].name.toLowerCase().indexOf(this.state.searchInput.toLowerCase()) >= 0) {
				results[bar] = bars[bar]
			}
		})
		
		this.setState({ searchResults: results })
	}

	handleChange = event => {
		this.setState({ searchInput: event.target.value },
		() => this.updateResults() )
	}

	render() {
		return (
			<div className="search">

				<h3 className="title">{copy.findEstablishment}</h3>

				<div className="search-input">
					<TextField
						id="search"
						label="Bar name"
						value={this.state.searchInput}
						onChange={this.handleChange}
						fullWidth
					/>
					<SearchIcon />
				</div>

				<div>
					{Object.keys(this.state.searchResults).map(bar =>
						<BrowseListItem
							data={this.state.searchResults[bar]}
							id={bar}
							key={bar}
						/>
					)}
				</div>
			</div>
		)
	}
}

export default Search
