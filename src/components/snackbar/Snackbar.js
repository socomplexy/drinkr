import React, { Component } from 'react'

import { copy } from '../../utils/copy'
import Button from 'material-ui/Button'
import Slide from 'material-ui/transitions/Slide'
import Snackbar from 'material-ui/Snackbar'

class SnackbarFooter extends Component {
	state = {
		open: true,
		direction: 'up'
	}

	createOrder = () => {
		this.props.formatOrder(this.props.order)
		this.props.history.push('/previous')
	}

	action = (
		<Button
			color="secondary"
			onClick={() => this.createOrder()}
			size="small"
		>
			{copy.completeOrder}
		</Button>
	)

	render () {
		return (
			<Snackbar
				open={this.state.open}
				onClose={this.props.close}
				transition={<Slide direction={this.state.direction} />}
				ContentProps={{ 'aria-describedby': 'message-id' }}
				message={<span id="message-id">{copy.totalLabel} {this.props.order.total.toFixed(2)}</span>}
				action={this.action}
			/>
		)
	}

}

export default SnackbarFooter
