import React from 'react'
import { Link } from 'react-router-dom'

import { copy } from '../../utils/copy'
import Button from 'material-ui/Button'
import './styles.css'

const Dashboard = () => (
  <div className="dashboard">

    <h3 className="title">{copy.dashboardTitle}</h3>

    <div className="dashboard-nav">
      <Link to="/search">
        <Button variant="raised" color="primary">
          {copy.search}
        </Button>
      </Link>

      <Link to="/browse">
        <Button variant="raised" color="secondary">
          {copy.browse}
        </Button>
      </Link>

      <Link to="/previous">
        <Button variant="raised">
          {copy.previousOrders}
        </Button>
      </Link>

    </div>
  </div>
)

export default Dashboard
