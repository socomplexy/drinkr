import React from 'react'

import bars from '../../data/bars.json'
import { copy } from '../../utils/copy'
import BrowseListItem from '../../components/browseListItem/BrowseListItem'

const Browse = () => (
	<div className="browse">

		<h3 className="title">{copy.browseBars}</h3>

		{Object.keys(bars).map(bar =>
			<BrowseListItem
				data={bars[bar]}
				id={bar}
				key={bar}
			/>
		)}
		
	</div>
)

export default Browse
