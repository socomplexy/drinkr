import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'

import { copy } from '../../utils/copy'
import Button from 'material-ui/Button'
import './styles.css'

const BrowseListItem = ({ data, id }) => (
	<div className='list-item'>

		<div className='image-container'>
			<img src={data.image} alt='' />
		</div>

		<div className='list-details'>
			<div className='bar-info'>
				<p className='bar-name'>
					{data.name}
					<span className='bar-location'> ({data.location})</span>
				</p>
				<p className='bar-description'>{data.description}</p>
			</div>

			<Link to={`/${id}`}>
				<Button variant="raised" color="secondary">
					{copy.viewMenu}
				</Button>
			</Link>

		</div>
	</div>
)

BrowseListItem.propTypes = {
	data: PropTypes.object.isRequired,
	id: PropTypes.string.isRequired,
}

export default BrowseListItem
