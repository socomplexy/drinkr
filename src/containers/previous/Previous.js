import React from 'react'
import moment from 'moment'

import bars from '../../data/bars.json'
import drinks from '../../data/drinks.json'
import { getSubtotal } from '../../utils'
import { copy } from '../../utils/copy'
import Button from 'material-ui/Button'
import './styles.css'


const Previous = ({ formatOrder, order }) => {
	// Previous orders, sorted by timestamp
	const previousOrders = order.previousOrders.sort((a, b) => b.time - a.time)

	return (
		<div className="history">
			<h3 className="title">{copy.previousOrders}</h3>

			<div className="history-list">

			{previousOrders.map(order =>
				<div key={order.time} id={order.time} className="previous-order-item">
					<div className="order-summary">
						<p>
							<span>{copy.barLabel}</span>
							{bars[order.bar].name}
						</p>
						<p>
							<span>{copy.dateLabel}</span>
							{moment(order.time).format("YYYY-MM-DD HH:mm")}
						</p>
						<p>
							<span>{copy.totalLabel}</span>
							{order.total.toFixed(2)}
						</p>
						<p>
							<span>{copy.itemsOrderedLabel}</span>
							{order.count}
						</p>
					</div>

					<div className="drink-summary">					
						{Object.keys(order.drinks).map(drink =>
							<div key={drink} className="drink-item">
								<p>
									<span>
										{drinks[drink].name} ({order.drinks[drink].quantity})
									</span>
								</p>
								<p>{getSubtotal(order.drinks[drink])}</p>
							</div>
						)}
					</div>

					<Button
						variant="raised"
						color="secondary"
						onClick={() => formatOrder(order)}
					>
						{copy.orderAgain}
					</Button>
				</div>
			)}
			</div>
		</div>
	)
}

export default Previous
