import { connect } from 'react-redux'
import Previous from './Previous'
import { formatOrder } from '../../actions'

const mapStateToProps = ({ order }) => ({ order })

const mapDispatchToProps = dispatch => ({
	formatOrder: order => dispatch(formatOrder(order)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Previous)
