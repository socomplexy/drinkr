import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import Bar from './Bar'
import {
	addDrink,
	formatOrder,
	populateDrink,
	removeDrink,
	selectBar
} from '../../actions'

const mapStateToProps = state => ({
	order: state.order,
})

const mapDispatchToProps = dispatch => ({
	addDrink: drink => dispatch(addDrink(drink)),
	formatOrder: order => dispatch(formatOrder(order)),
	populateDrink: drink => dispatch(populateDrink(drink)),
	removeDrink: drink => dispatch(removeDrink(drink)),
	selectBar: bar => dispatch(selectBar(bar)),
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Bar))
