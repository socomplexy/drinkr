import React, { Component } from 'react'

import bars from '../../data/bars.json'
import drinks from '../../data/drinks.json'
import { copy } from '../../utils/copy' 

import SnackbarFooter from '../../components/snackbar/Snackbar'
import Add from 'material-ui-icons/Add'
import Button from 'material-ui/Button'
import Remove from 'material-ui-icons/Remove'
import './styles.css'

class Bar extends Component {
	state = {
		menu: [],
		showSnackbar: false,
	}

	// Select the bar object based on the url param
	bar = bars[this.props.match.params.bar]

	populateMenu = () => {
		let results = []
		this.props.selectBar(this.props.match.params.bar)

		Object.keys(this.bar.menu).forEach(drink => {
			const drinkId = this.bar.menu[drink].drinkId

			const menuItem = {
				id: drinkId,
				price: this.bar.menu[drink].price,
				name: drinks[drinkId].name,
				image: drinks[drinkId].image,
				description: drinks[drinkId].description,
			}

			results.push(menuItem)
			this.props.populateDrink(menuItem)
		})

		this.setState({menu: results})
	}

	addDrink = item => {
		this.props.addDrink(item)
		this.setState({ showSnackbar: true })
	}

	removeDrink = item => {
		if(this.props.order.drinks[item.id].quantity > 0) {
			this.props.removeDrink(item)
		}
		this.setState({ showSnackbar: true })
	}

	handleCloseSnackbar = () => this.setState({ showSnackbar: false })

	componentDidMount() { this.populateMenu() }

	render() {
		const { formatOrder, order, history } = this.props

		return (
			<div className="bar-container">
				<div className='bar-info'>
					<div className='bar-image-container'>
						<img src={this.bar.image} alt='' />
					</div>

					<div>
						<h3 className='title'>
							{this.bar.name}
						</h3>
						<p className='bar-description'>
							{this.bar.description}
						</p>
					</div>
				</div>
				
				<div className="menu">
					<h3 className="title">{copy.menu}</h3>

					<div className='drink-list'>
						{this.state.menu.map(item =>
							<div className='drink-item' key={item.id}>
								<div className="item-actions">
									<Button
										variant="raised"
										onClick={() => this.removeDrink(item)}
									>
										<Remove />
									</Button>
									<p className="item-qty">
										{this.props.order.drinks[item.id].quantity}
									</p>
									<Button
										variant="raised"
										onClick={() => this.addDrink(item)}
									>
										<Add />
									</Button>
								</div>

								<p className='item-name'>
									{item.name}
								</p>
								<p className='item-price'>
									{item.price.toFixed(2)}
								</p>

								<div className='image-container'>
									<img src={item.image} alt='' />
								</div>

								<p className='item-description'>
									{item.description}
								</p>
							</div>
						)}
					</div>
				</div>

				{this.state.showSnackbar && (
					<SnackbarFooter
						formatOrder={formatOrder}
						order={order}
						history={history}
						close={this.handleCloseSnackbar}
						open={this.state.showSnackbar}
					/>
				)}

			</div>
		)
	}
}

export default Bar
