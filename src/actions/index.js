import {
	DRINK_ADDED,
	DRINK_CREATED,
	DRINK_REMOVED,
	BAR_SELECTED,
	ORDER_COMPLETED
} from '../utils/constants'

export const addDrink = drink => ({
	type: DRINK_ADDED,
	drink
})

export const populateDrink = drink => ({
	type: DRINK_CREATED,
	drink
})

export const removeDrink = drink => ({
	type: DRINK_REMOVED,
	drink
})

export const selectBar = bar => ({
	type: BAR_SELECTED,
	bar
})

export const completeOrder = order => ({
	type: ORDER_COMPLETED,
	order
})

export const formatOrder = order => dispatch => {
	const { drinks, bar, total, count } = order

	let drinksAdded = {}
	
	// Only add drinks with a qty greater than 0
	Object.keys(drinks).forEach(drink => {
		if(drinks[drink].quantity > 0) {
			drinksAdded[drink] = drinks[drink]
		}
	})

	const result = {
		drinks: drinksAdded,
		time: Date.now(),
		bar,
		total,
		count,
	}

	dispatch(completeOrder(result))
}
